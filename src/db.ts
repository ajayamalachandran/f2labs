import {createConnection} from 'typeorm';
// import {User,common,JwtToken,Exam,Subject,Topic,Chapter,Question} from './entity';
import {Chapter} from './entity/chapter.entity';
import {common} from './entity/common.entity';
import {Exam} from './entity/exam.entity';
import {JwtToken} from './entity/jwtToken.entity';
import {Question} from './entity/question.entity';
import {Subject} from './entity/subject.entity';
import {Topic} from './entity/topic.entity';
import {User} from './entity/user.entity';
import {StudentQuestion} from './entity/studentquestion.entity'


export const dbConnection= async ()=>{
    const DATABASE_HOST=process.env.DATABASE_HOST || 'localhost';
    const DATABASE_PORT=5432;
    const DATABASE_USER=process.env.DATABASE_USER || "postgres";
    const DATABASE_PASSWORD='psql';
    const DATABASE_NAME= 'ajay_local';
    const entities=[
        StudentQuestion,
        Question,
        User,
        common, 
        JwtToken,
        Exam,
        Subject,
        Topic,
        Chapter,
    ]
    const connection= await createConnection({
        type:'postgres',
        host:DATABASE_HOST,
        port:DATABASE_PORT,
        username:DATABASE_USER,
        password:DATABASE_PASSWORD,
        database:DATABASE_NAME,
        synchronize: true,
        entities:entities
    }).then((value)=>{
    }).catch(async (error)=>{
        return await createConnection({
            type:'postgres',
            host:DATABASE_HOST,
            port:DATABASE_PORT,
            username:DATABASE_USER,
            password:DATABASE_PASSWORD,
            database:DATABASE_NAME,
            synchronize: true,
            entities:entities

    })
})
    return connection;

}
