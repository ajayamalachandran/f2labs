import {Request,Response,NextFunction} from 'express';
import {verify,verifyError} from 'jsonwebtoken'
import {JwtService} from '../services';
import {BaseHttpController} from 'inversify-express-utils'
export const AuthMiddlewere= () =>{
    return (request:Request,response:Response, next: NextFunction )=>{
        (async ()=>{
            const routepath =request.route.path;
            const header=request.headers;
            if(request.headers.authorization!==null && request.headers.authorization!==undefined){
                if(new JwtService().isValidToken(request.headers.Authorization)){
                    verify(request.headers.authorization,'secrect',undefined,(err:verifyError,decodedObj:Object)=>{
                        if(err || !decodedObj){
                            return response.status(401).json({
                                status:401,
                                message:"Unauthorized"
                            }) 
                        }else {
                            next();
                        }
                    })
                }else {
                    return response.status(401).json({
                        status:401,
                        message:"Unauthorized"
                    }) 
                }
            }else{
                return response.status(401).json({
                    status:401,
                    message:"Unauthorized"
                }) 
            }
        })();
    }
}
const authMiddlewere=AuthMiddlewere();

export {authMiddlewere}