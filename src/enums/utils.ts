enum appRoutes{
    USER='/user',
    ROOT='/api',
    EXAM='/exam',
    SUBJECT='/subject',
    TOPIC='/topic',
    CHAPTER='/chapter',
    QUESTION='/question',
    STUDENTQUSTION='/studentmeta',
    REPORTS='/report'
}
enum answerType{
    UNANSWERD,
    RIGHTANSWER,
    WRONGANSWER,
    SKIPPEDANSWER
}
enum questionType{
    EASY,
    MEDIUM,
    HARD
}
export {appRoutes,answerType,questionType}