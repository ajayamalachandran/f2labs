

import {Repository, In} from 'typeorm';
import {BaseHttpController,httpGet,httpPost,httpDelete,httpPut,controller} from 'inversify-express-utils';
import { appRoutes,questionType,answerType } from '../enums/utils';
import { authMiddlewere } from '../middlewares/authendication.middleware';
import { JsonResult } from 'inversify-express-utils/dts/results';
import { inject, id } from 'inversify';
import { TYPE } from '../constants/types';
import { StudentQuestion,Question } from '../entity';
import { OK, NOT_FOUND, INTERNAL_SERVER_ERROR } from 'http-status-codes';
@controller(appRoutes.REPORTS,authMiddlewere)
export class ReportsController extends BaseHttpController{
    @inject(TYPE.StudentQuestionRepository) private readonly _studentQuestionRepository:Repository<StudentQuestion>
    @inject(TYPE.QuestionRepository) private readonly _questionRepository:Repository<Question>
   
    @httpGet('/')
    public async getReports():Promise<JsonResult>{
        var params=this.httpContext.request.query;
        console.log(params);
        console.log({[params['report']]:params[params['report']]})
        return this._questionRepository.find({where:{[params['report']]:params[params['report']]}}).then(async (response)=>{
    if(response){
        let resultedQuestions=response.map((item)=> item['id']);
        console.log(resultedQuestions)
       return  this._studentQuestionRepository.find({where:{question:In(resultedQuestions)}}).then(async (reportData)=>{
            if(reportData){
                console.log(reportData)
                var answered=((reportData.filter((item)=> item.studentAnswer===answerType.RIGHTANSWER).length)/(resultedQuestions.length))*100; 
                var wronganswer=((reportData.filter((item)=> item.studentAnswer===answerType.WRONGANSWER).length)/(resultedQuestions.length))*100;
                var skippedQuestions=((reportData.filter((item)=> item.studentAnswer===answerType.SKIPPEDANSWER).length)/(resultedQuestions.length))*100;
                var unanswerd=100-(answered+wronganswer+skippedQuestions);
                console.log(answered,wronganswer,skippedQuestions,unanswerd)
                return await this.json({
                    result:{
                        answered:answered,
                        unanswerd:unanswerd,
                        wronganswer:wronganswer,
                        skippedQuestions:skippedQuestions
                    }
                },OK)
               
            }else {
                this.json({
                    message:'No Date Found'
                },NOT_FOUND)
            }
        }).catch((error)=>{
            return this.json({
                error
            },INTERNAL_SERVER_ERROR)
        })
       

    }
      
    })
}
}



