import { Repository } from 'typeorm';
import { BaseHttpController, httpGet, httpPost, httpDelete, httpPut, controller } from 'inversify-express-utils';
import { appRoutes } from '../enums/utils';
import { authMiddlewere } from '../middlewares/authendication.middleware';
import { JsonResult } from 'inversify-express-utils/dts/results';
import { inject, id } from 'inversify';
import { TYPE } from '../constants/types';
import { Chapter } from '../entity';
import { OK, NOT_FOUND, INTERNAL_SERVER_ERROR } from 'http-status-codes';
@controller(appRoutes.CHAPTER, authMiddlewere)
export class ChapterController extends BaseHttpController {
    @inject(TYPE.ChapterRepository) private readonly _chapterRepository: Repository<Chapter>
    @httpPost('/create')
    public async createChapter(): Promise<JsonResult> {
        let body = this.httpContext.request.body;
        return await this._chapterRepository.createQueryBuilder().insert().into(Chapter).values(body).execute().then((result) => {
            if (result) {
                return this.json({
                    data: result
                },OK)
            } else {
                return this.json({
                    message: "No Data Found",
                    data: result
                },NOT_FOUND)
            }
        }).catch((error) => {
            return this.json({
                error: error,
                message: 'Internel server error'
            },INTERNAL_SERVER_ERROR)
        })
    }
    @httpGet('/:id?')
    public async getChapter(): Promise<JsonResult> {
        var queryParams = this.httpContext.request.query
        var { id } = this.httpContext.request.params;
        var body = this.httpContext.request.body;
        return await this._chapterRepository.find({ where: queryParams }).then((result) => {
            if (result) {
                return this.json({ result },OK)
            } else {
                return this.json({ message: 'No Data Found' },NOT_FOUND)
            }
        }).catch((error) => {
            return this.json({
                message: 'Query Failed',
                error: error
            },INTERNAL_SERVER_ERROR)
        })
    }
}