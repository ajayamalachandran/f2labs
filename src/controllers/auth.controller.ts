import { inject } from 'inversify';
import { Repository, InsertResult } from 'typeorm'
import { httpGet, httpPost, httpPut, controller, BaseHttpController } from 'inversify-express-utils';
import { appRoutes } from '../enums/utils';
import { TYPE } from '../constants/types';
import { User } from '../entity';
import { JwtToken } from '../entity/jwtToken.entity';
import { JsonResult } from 'inversify-express-utils/dts/results';
import { sign } from 'jsonwebtoken';
import { authMiddlewere } from '../middlewares/authendication.middleware';
import { JwtService } from '../services/token.service';
import * as CryptoJS from 'crypto-js'
import { OK, NOT_FOUND, INTERNAL_SERVER_ERROR, UNAUTHORIZED } from 'http-status-codes';
@controller(appRoutes.USER)
export class AuthController extends BaseHttpController {
    @inject(TYPE.UserRepository) private readonly _userRepository: Repository<User>;
    @inject(TYPE.JwtRepository) private readonly _jwtTokenRepositort: Repository<JwtToken>;
    @inject(TYPE.JwtTokenService) private readonly _jwtTokenService: JwtService;
    key = CryptoJS.enc.Hex.parse("000102030405060708090a0b0c0d0e0f");
    iv = CryptoJS.enc.Hex.parse("101112131415161718191a1b1c1d1e1f");
    @httpPost('/signup')
    public async signUp(): Promise<JsonResult> {
        let body = this.httpContext.request.body;
        body['email']=body['email'].toString().toLowerCase();
        body['password'] = await this.encrypt(this.httpContext.request.body['password'])
        return await this._userRepository.save(this.httpContext.request.body).then((result) => {
            console.log(result);
            if (result) {
                return this.json({
                    result,
                    message: "User updated Successfully"
                },OK)
            } else {
                return this.json({
                    error: 'No Data Found',
                },NOT_FOUND)
            }
        }).catch((error) => {
            console.log(error);
            return this.json({
                error: error
            },INTERNAL_SERVER_ERROR)
        })
    }
    @httpPost('/signin')
    public async signIn(): Promise<JsonResult> {
        var email = this.httpContext.request.body.email;
        var password = await this.encrypt(this.httpContext.request.body['password']);
        return await this._userRepository.findOne({ where: { email: email, password: password } }).then(async (user) => {
            if (user) {
                let payload = {
                    firstName: user.firstName,
                    lastName: user.lastName,
                    email: user.email,
                    dob: user.dob,
                }
                let encodedToken: string = sign(payload, 'secrect', { expiresIn: '2d' });
                let Obj = {
                    jwt: encodedToken,
                    user: user.id
                }
                let jwtToken = new JwtToken();
                jwtToken.jwtToken = encodedToken;
                jwtToken.user = user.id
                return await this._jwtTokenRepositort.save(jwtToken).then((result) => {
                    if (result) {
                        return this.json({
                            user,
                            encodedToken
                        },OK)
                    }

                }).catch((error) => {
                    return this.json({
                        error: error,
                        message: 'Internel Server Error'
                    },INTERNAL_SERVER_ERROR)
                })

            } else {
                return this.json({
                    status: 401,
                    message: 'Username and Password are Incorrect'
                },UNAUTHORIZED)
            }
        }).catch((error: Error) => {
            console.log(error)
            return this.json({
                error: 'Internal server Error'
            },INTERNAL_SERVER_ERROR)
        })
        // return await this._userRepository.createQueryBuilder().insert().into(User).values(this.httpContext.request.body).execute().then((result:InsertResult)=>{
        //     console.log(result);
        //     return this.json({
        //         message:'User Saved Successfully',
        //         status:200
        //     })
        // })
    }
    @httpPost('/signout')
    public async Signout(): Promise<JsonResult> {
        var data = this.httpContext.request.headers['authorization'];
        return await this._jwtTokenService.invalidateToken(data);
    }
    async encrypt(plain_text: string): Promise<string> {
        return await CryptoJS.AES.encrypt(plain_text, this.key, { iv: this.iv }).toString()
    };
    async decrypt(cipher_text: string): Promise<string> {
        return await CryptoJS.AES.decrypt(cipher_text, this.key, { iv: this.iv }).toString(CryptoJS.enc.Utf8)
    };

}