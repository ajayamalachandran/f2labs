import { Repository, Not, In } from 'typeorm';
import { BaseHttpController, httpGet, httpPost, httpDelete, httpPut, controller } from 'inversify-express-utils';
import { appRoutes, answerType } from '../enums/utils';
import { authMiddlewere } from '../middlewares/authendication.middleware';
import { JsonResult } from 'inversify-express-utils/dts/results';
import { inject, id } from 'inversify';
import { TYPE } from '../constants/types';
import { Question, StudentQuestion } from '../entity';
import * as XLSX from "xlsx";
import { OK, NOT_FOUND, INTERNAL_SERVER_ERROR } from 'http-status-codes';
@controller(appRoutes.QUESTION, authMiddlewere)
export class QuestionController extends BaseHttpController {
    @inject(TYPE.QuestionRepository) private readonly _questionRepository: Repository<Question>
    @inject(TYPE.StudentQuestionRepository) private readonly _studentQuestionRepository: Repository<StudentQuestion>
    @httpPost('/create')
    public async createQuestion(): Promise<JsonResult> {
        let body = this.httpContext.request.body;
        return await this._questionRepository.createQueryBuilder().insert().into(Question).values(body).execute().then((result) => {
            if (result) {
                return this.json({
                    data: result
                },OK)
            } else {
                return this.json({
                    message: "No Data Found",
                },NOT_FOUND)
            }
        }).catch((error) => {
            return this.json({
                error: error,
                message: 'Internel server error'
            },INTERNAL_SERVER_ERROR)
        })
    }

    @httpPost('/convert')
    public async convert(): Promise<JsonResult> {
        let workbookSheet = XLSX.readFile(this.httpContext.request['files'][0].path);
        let jsonObj = XLSX.utils.sheet_to_json(
            workbookSheet.Sheets[workbookSheet.SheetNames[0]]
        );
        if (jsonObj.length > 0) {
            return this.json({
                jsonObj
            },OK);
        } else {
            return this.json({
                message: 'Convertion Failed'
            },INTERNAL_SERVER_ERROR)
        }
    }

    @httpGet('/:id?')
    public async getQuestions(): Promise<JsonResult> {
        var queryParams = this.httpContext.request.query
        var { id } = this.httpContext.request.params;
        var body = this.httpContext.request.body;
        if (queryParams['testStatus'] === undefined || !queryParams['testStatus']) {
            delete queryParams['user'];
            return await this._questionRepository.find({ where: queryParams }).then(async (result) => {
                if (result) {

                    return this.json({
                        result
                    },OK)

                } else {
                    return this.json({
                        message: "No Data Found"
                    },NOT_FOUND)
                }

            }).catch((error) => {
                return this.json({
                    message: 'Query Failed',
                    error: error
                },INTERNAL_SERVER_ERROR)
            })
        } else {
            return await this._studentQuestionRepository.find({ where: { user: queryParams['user'] } }).then(async (answer) => {
                if (answer) {
                    let result = []
                    let questionArray = answer.map(item => item['question']['id']);
                    delete queryParams['user'];
                    delete queryParams['testStatus'];
                    delete queryParams['question'];
                    return await this._questionRepository.find({ where: queryParams }).then((sampleresult) => {
                        let questionRepo = sampleresult.map(item => item['id']);
                        if (sampleresult) {

                            for (let question of sampleresult) {
                                if (!questionArray.includes(question['id'])) {
                                    result.push(question)
                                }
                            }

                            if (result.length > 0) {
                                return this.json({
                                    result
                                },OK)
                            } else if (result.length === 0) {
                                for (let question of answer) {
                                    if (questionRepo.includes(question['question']['id']) && question['studentAnswer'] === answerType.SKIPPEDANSWER) {
                                        result.push(question['question'])
                                    }
                                }
                                return this.json({
                                    result
                                },OK)
                            }
                            else {
                                return this.json({
                                    message: 'No Data Found'
                                },NOT_FOUND)
                            }
                        }
                    }).catch((error) => {
                        return this.json({
                            message: "Query Builder Error",
                            error: error
                        },INTERNAL_SERVER_ERROR)
                    })
                }
            })
        }
    }
}