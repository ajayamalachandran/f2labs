import { Repository } from 'typeorm';
import { BaseHttpController, httpGet, httpPost, httpDelete, httpPut, controller } from 'inversify-express-utils';
import { appRoutes } from '../enums/utils';
import { authMiddlewere } from '../middlewares/authendication.middleware';
import { JsonResult } from 'inversify-express-utils/dts/results';
import { inject } from 'inversify';
import { TYPE } from '../constants/types';
import { Exam } from '../entity';
import { OK, NOT_FOUND, INTERNAL_SERVER_ERROR } from 'http-status-codes';
@controller(appRoutes.EXAM, authMiddlewere)
export class ExamController extends BaseHttpController {
    @inject(TYPE.ExamRepository) private readonly _examRepository: Repository<Exam>
    @httpPost('/create')
    public async createExam(): Promise<JsonResult> {
        let body = this.httpContext.request.body;
        return await this._examRepository.createQueryBuilder().insert().into(Exam).values(body).execute().then((result) => {
            if (result) {
                return this.json({
                    data: result
                },OK)
            } else {
                return this.json({
                    message: "No Data Found",
                },NOT_FOUND)
            }
        }).catch((error) => {
            return this.json({
                error: error,
                message: 'Internel server error'
            },INTERNAL_SERVER_ERROR)
        })
    }
    @httpGet('/:id?')
    public async getExams(): Promise<JsonResult> {
        return await this._examRepository.find().then((result) => {
            if (result) {
                return this.json({ result },OK)
            } else {
                return this.json({ message: 'No Data Found' },NOT_FOUND)
            }
        }).catch((error) => {
            return this.json({
                message: 'Query Failed Error',
                error: error
            },INTERNAL_SERVER_ERROR)
        })
    }
}