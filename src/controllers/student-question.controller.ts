import { Repository, In } from 'typeorm';
import { BaseHttpController, httpGet, httpPost, httpDelete, httpPut, controller } from 'inversify-express-utils';
import { appRoutes, questionType, answerType } from '../enums/utils';
import { authMiddlewere } from '../middlewares/authendication.middleware';
import { JsonResult } from 'inversify-express-utils/dts/results';
import { inject, id } from 'inversify';
import { TYPE } from '../constants/types';
import { StudentQuestion, Question } from '../entity';
import { OK, NOT_FOUND,INTERNAL_SERVER_ERROR } from "http-status-codes";

@controller(appRoutes.STUDENTQUSTION, authMiddlewere)
export class StudentQuestionController extends BaseHttpController {
    @inject(TYPE.StudentQuestionRepository) private readonly _studentQuestionRepository: Repository<StudentQuestion>
    @inject(TYPE.QuestionRepository) private readonly _questionRepository: Repository<Question>
    //     @httpPost('/create')
    //     public async createExam():Promise<JsonResult>{
    //         let body=this.httpContext.request.body;
    //         console.log(body);
    //        return await this._studentQuestionRepository.createQueryBuilder().insert().into(StudentQuestion).values(body).execute().then((result)=>{
    //            if(result){
    //                return this.json({
    //                    message:"Qujesry Build Successfully",
    //                    data:result
    //                })
    //            }else {
    //             return this.json({
    //                 message:"Unknown Error",
    //                 data:result
    //             })
    //            }
    //        }).catch((error)=>{
    //            console.log(error);
    //            return this.json({
    //                error:error,
    //                message:'Internel server error'
    //            })
    //        })
    // }


    @httpGet('/:id?')
    public async getstudentQuestions(): Promise<JsonResult> {
        var queryParams = this.httpContext.request.query
        var { id } = this.httpContext.request.params;
        var body = this.httpContext.request.body;
        return await this._studentQuestionRepository.find().then((result) => {
            if (result) {
                return this.json({
                    result
                },OK)
            } else {
                return this.json({
                    message: 'No Data Found',
                    
                },NOT_FOUND)
            }
        }).catch((error) => {
            return this.json({
                error: error,
                message: 'Internal Server Error'
            },INTERNAL_SERVER_ERROR)
        })
    }
    @httpPost('/report')
    public async getReport(): Promise<JsonResult> {
        var body = this.httpContext.request.body;
        console.log(body);
        return await this._studentQuestionRepository.find({ where: { user: body['user'] } }).then(async (questions) => {
            if (questions) {
                // return this.json({questions});
                let questionArray = [...Array.from(new Set(questions.map((item) => item['question']['id'])))].length;
                let chapterArray = [...Array.from(new Set(questions.map((item) => item['question']['chapter']['id'])))].length;
                let topicArray = [...Array.from(new Set(questions.map((item) => item['question']['topic']['id'])))].length;
                let subjectArray = [...Array.from(new Set(questions.map((item) => item['question']['subject']['id'])))].length;
                let examArray = [...Array.from(new Set(questions.map((item) => item['question']['exam']['id'])))].length;
                // console.log(questionArray,chapterArray,topicArray,subjectArray,examArray)

                return this.json({
                    question: questionArray,
                    chapter: chapterArray,
                    topic: topicArray,
                    subject: subjectArray,
                    exam: examArray
                },OK)
            } else {
                return this.json({
                    message: 'No Data Found'
                },NOT_FOUND)
            }
        }).catch((error) => {
            return this.json({
                message: "Query Builder Error",
                error: error
            },INTERNAL_SERVER_ERROR)
        })
    }
    @httpPost('/check')
    public async checkAnswer(): Promise<JsonResult> {
        var body = this.httpContext.request.body;
        return await this._questionRepository.findOne({ where: { id: body['id'] } }).then(async (result) => {

            if (result) {
                let answerCode = 0;
                if (result['answer'].trim().toString() === body['answer'].trim().toString()) {
                    answerCode = answerType.RIGHTANSWER;
                } else if (body['answer'] === "") {
                    answerCode = answerType.SKIPPEDANSWER;
                } else {
                    answerCode = answerType.WRONGANSWER;
                }
                let answerObject = {}
                answerObject['user'] = body['user'];
                answerObject['question'] = result['id'];
                answerObject['studentAnswer'] = answerCode;
                return await this._studentQuestionRepository.findOne({ where: { question: result['id'] } }).then((data) => {
                    if (data) {
                        return this._studentQuestionRepository.createQueryBuilder().update(StudentQuestion).set({ studentAnswer: answerCode }).where("question=:question", { question: result['id'] }).execute().then((saveresult) => {
                            if (saveresult) {
                                return this.json({
                                    saveresult
                                },OK)
                            }else {
                                return this.json({
                                    message:'No Data Found'
                                },NOT_FOUND)
                            }
                        }).catch((error) => {
                            return this.json({
                                error: error,
                                message: "Query Builder Error"
                            },INTERNAL_SERVER_ERROR)
                        })
                    } else {
                        return this._studentQuestionRepository.createQueryBuilder().insert().into(StudentQuestion).values(answerObject).execute().then((saveResult) => {
                            if (saveResult) {
                                return this.json({
                                    saveResult
                                },OK)
                            } else {
                                return this.json({
                                    message: 'No result Found'
                                },NOT_FOUND)
                            }

                        }).catch((error) => {
                            return this.json({
                                error: error,
                                message: 'Query Builder Error'
                            },INTERNAL_SERVER_ERROR)

                        })

                    }

                })

            } else {
                return this.json({ message: 'Query returns null' },NOT_FOUND)
            }
        }).catch((error) => {
            return this.json({
                message: 'Query Failed',
                error: error
            },INTERNAL_SERVER_ERROR)
        })
    }


}


//     return this._questionRepository.find({where:{[body['report']]:body[body['report']]}}).then((response)=>{
    // if(response){
    //     let resultedQuestions=response.map((item)=> item['id']);
    //     console.log(resultedQuestions);
    //     this._studentQuestionRepository.find({where:{question:In(resultedQuestions)}}).then((reportData)=>{
    //         if(reportData){
    //             var answered=reportData.filter((item)=> item.studentAnswer===answerType.RIGHTANSWER).length;
    //             var unanswerd=reportData.filter((item)=> item.studentAnswer===answerType.UNANSWERD).length;
    //             var wronganswer=reportData.filter((item)=> item.studentAnswer===answerType.WRONGANSWER).length;
    //             var skippedQuestions=reportData.filter((item)=> item.studentAnswer===answerType.SKIPPEDANSWER).length;

    //             return this.json({
    //                 result:{
    //                     answered:answered,
    //                     unanswerd:unanswerd,
    //                     wronganswer:wronganswer,
    //                     skippedQuestions:skippedQuestions
    //                 }
    //             })

    //         }else {
    //             this.json({
    //                 message:'No Date Found'
    //             })
    //         }
    //     }).catch((error)=>{
    //         console.log(error);
    //         return this.json({
    //             error
    //         })
    //     })
    //     return this.json({

    //     })

    // }

    // })