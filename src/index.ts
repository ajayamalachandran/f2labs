import "reflect-metadata";
import { Container } from "inversify";
import * as express from 'express';
import * as cors from  'cors';
import * as bodyParser from 'body-parser';
import  {InversifyExpressServer} from 'inversify-express-utils';
import * as os from 'os';
import {appRoutes} from './enums/utils'
import { bindings } from "./inversity.engineConfig";
import * as multer from "multer";
import * as mkdirp from "mkdirp";
import moment = require("moment");

var storage = multer.diskStorage({
    destination: (req, file, cb) => {
      mkdirp(__dirname + '/public/uploads/' + moment().format('MMMM_Do_YYYY'), function (err) {
        cb(null, __dirname + '/public/uploads/' + moment().format('MMMM_Do_YYYY'))
      })
  
    },
    filename: (req, file, cb) => {
      let fName = file.originalname.split(".")[1] + "-";
      cb(null, fName + Date.now() + '.' + file.originalname.split(".")[1]);
    }
  });
  
  var upload = multer({ storage: storage });

(async  () => {

    const port=4000;
    const container=new Container();
    await container.loadAsync(bindings);
    const app=new InversifyExpressServer(container,null,{rootPath:appRoutes.ROOT})
    app.setConfig((appConfig)=>{
        appConfig.use(express.static(__dirname + '/public'))
        appConfig.use(cors())
        appConfig.use(upload.any());
        appConfig.use(bodyParser.urlencoded({extended:true,limit:'50mb'}))
        appConfig.use(bodyParser.json({limit:'50mb'}))
    })
    const server=app.build();
    server.listen(port,()=>{
        for (let addresses of Object.values(os.networkInterfaces())) {
            for (let add of addresses) {
                console.log(`Server running at ${add.address}:${port}/`);
                return;
            }
          }
    })
})();
