import {Entity, Column, ManyToOne, OneToMany, JoinColumn} from 'typeorm';
import {Subject} from  '../entity/subject.entity';
import {Topic} from  '../entity/topic.entity';
import {common} from  '../entity/common.entity';
import {Exam} from  '../entity/exam.entity';
import {Question} from  '../entity/question.entity';

@Entity()
export class Chapter extends common{
    @Column({type:'varchar'})
    chapterName:String;
    @ManyToOne(type => Exam,exam=> exam.chapters,{eager: true})
    @JoinColumn({
        name:'exam'
    })
    exam:Exam;
    @ManyToOne(type => Subject,subject=> subject.chapters,{eager: true})
    @JoinColumn({
        name:'subject'
    })
    subject:Subject;
    @ManyToOne(type=>Topic,topic=>topic.chapters,{eager: true})
    @JoinColumn({
        name:'topic'
    })
    topic:Topic[];
    @OneToMany(type=>Question,question=>question.chapter)
    questions:Question[];
}
