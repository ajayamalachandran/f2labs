import {PrimaryGeneratedColumn,Column} from 'typeorm';
export abstract class common {
    @PrimaryGeneratedColumn('uuid')
    id!:number;
    @Column({nullable:true,type:'integer',default:1})
    status:number
    @Column({ type: "timestamp with time zone", default: () => "CURRENT_TIMESTAMP" })
    createdAt:Date
    @Column({type :"timestamp with time zone", default: ()=> "CURRENT_TIMESTAMP" })
    updatedAt:Date
    @Column({type:"time with time zone" , default:()=> "CURRENT_TIMESTAMP"})
    deletedAt:Date
}