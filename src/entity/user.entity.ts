import {Entity, Column, OneToMany, ManyToMany} from "typeorm";
import {common} from '../entity';
import { JwtToken } from "./jwtToken.entity";
import { StudentQuestion } from "./studentquestion.entity";
// import { JwtToken } from "./jwtToken.entity";

@Entity()
export class User extends common {
    @Column({type: 'varchar'})
    firstName: string;

    @Column({type: 'varchar' ,nullable:true})
    lastName: string;

    @Column({type:'varchar' ,unique:true})
    email:string;

    @Column({type:'varchar'})
    password!:string;

    @Column({type:'date',nullable:true})
    dob: Date;

    @Column({type:'varchar',nullable:true})
    gender: string;
    
    @Column({type:'varchar',nullable:true})
    contactNo:string;

    @Column({type:'varchar',nullable:true})
    verificationCode:string;

    @Column({type:'integer'})
    role:number;
    
    @OneToMany(type => JwtToken, jwt => jwt.user)
    createdUsers: JwtToken[];

    @OneToMany(type => StudentQuestion, studentQuest => studentQuest.user)
    addedUsers: StudentQuestion[];

}
