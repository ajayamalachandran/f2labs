import { Entity, Column, ManyToOne, OneToMany, JoinColumn } from 'typeorm';
import {Chapter} from  '../entity/chapter.entity';
import {Exam} from  '../entity/exam.entity';
import {Topic} from  '../entity/topic.entity';
import {common} from  '../entity/common.entity';
import {Question} from  '../entity/question.entity';

@Entity()
export class Subject extends common {
    @Column({type:'varchar'})
    subjectName:String;
    @ManyToOne(type => Exam,exam=> exam.subjects,{eager: true})
    @JoinColumn({
        name:'exam'
    })
    exam:Exam;
    @OneToMany(type=>Topic,topic=>topic.subject)
    topics:Topic[];
    @OneToMany(type=>Chapter,chapter=>chapter.subject)
    chapters:Chapter[];
    @OneToMany(type=>Question,question=>question.subject)
    questions:Question[];
}