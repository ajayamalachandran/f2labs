import {Entity, Column, ManyToOne, OneToMany, JoinColumn} from 'typeorm';
import { Chapter } from '../entity/chapter.entity';
import { Exam } from '../entity/exam.entity'
import { Subject } from '../entity/subject.entity'
import { Question } from '../entity/question.entity'
import { common } from '../entity/common.entity'
@Entity()
export class Topic extends common {
    @Column({type:'varchar'})
    topicName:String
    @ManyToOne(type => Exam,exam=> exam.topics ,{eager: true})
    @JoinColumn({
        name:'exam'
    })
    exam:Exam;
    @ManyToOne(type => Subject,subject=> subject.topics,{eager: true})
    @JoinColumn({
        name:'subject'
    })
    subject:Subject;
    @OneToMany(type=>Chapter,chapter=>chapter.topic)
    chapters:Chapter[];
    @OneToMany(type=>Question,question=>question.topic)
    questions:Question[];
}
