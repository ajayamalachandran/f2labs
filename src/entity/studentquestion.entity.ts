import {Entity, Column, ManyToOne, JoinColumn} from 'typeorm';
import {common} from '../entity'
import { User } from './user.entity';
import { JwtToken } from './jwtToken.entity';
import { Question } from './question.entity';
import {answerType} from '../enums/utils'

@Entity()

export class StudentQuestion extends common {
    @Column({type:'integer',default:answerType.UNANSWERD})
    studentAnswer:number;
    @ManyToOne(type => Question,question=> question.student,{eager: true})
    @JoinColumn({
        name: "question"
    })
    question:Question
    @ManyToOne(type => User,user=> user.addedUsers,{eager: true})
    @JoinColumn({
        name: "user"
    })
    user: User;
    // @Column({type:'varchar'})
    // jwtToken:string
}
    