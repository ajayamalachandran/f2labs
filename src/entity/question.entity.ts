import {Entity, Column, ManyToOne, OneToMany, JoinColumn} from 'typeorm';
import {Chapter} from  '../entity/chapter.entity';
import {Subject} from  '../entity/subject.entity';
import {Topic} from  '../entity/topic.entity';
import {common} from  '../entity/common.entity';
import {Exam} from  '../entity/exam.entity';
import {answerType,questionType} from '../enums/utils'
import { StudentQuestion } from './studentquestion.entity';

@Entity()
export class Question extends common {
    @Column({type:'varchar',nullable:true})
    question:string;
    @Column({type:'varchar'})
    option1:string;
    @Column({type:'varchar'})
    option2:string;
    @Column({type:'varchar'})
    option3:string;
    @Column({type:'varchar'})
    option4:string;
    @Column({type:'varchar',nullable:true})
    hint:string;
    @Column({type:'integer'})
    questionType:number;
    @Column({type:'varchar'})
    answer:string;
    @Column({type:'integer',default:answerType.UNANSWERD})
    answerType:number
    @ManyToOne(type => Exam,exam=> exam.questions,{eager: true,})
    @JoinColumn({
        name:'exam'
    })
    exam:Exam;
    @ManyToOne(type => Subject,subject=> subject.questions ,{eager: true})
    @JoinColumn({
        name:'subject'
    })
    subject:Subject;
    @ManyToOne(type=>Topic,topic=>topic.questions,{eager: true})
    @JoinColumn({
        name:'topic'
    })
    topic:Topic;
    @ManyToOne(type=>Chapter,chapter=>chapter.questions,{eager: true})
    @JoinColumn({
        name:'chapter'
    })
    chapter:Chapter;
    @OneToMany(type=>StudentQuestion,student=>student.question)
    student:StudentQuestion[]
}
