import {Entity,Column, OneToMany, ManyToOne, JoinColumn} from 'typeorm'; 
import {common} from './common.entity';
import {User} from './user.entity';

@Entity()
export class JwtToken extends common {
    @Column({type:'varchar',nullable:true,unique:true})
    jwtToken:string
    @ManyToOne(type=> User, owndUser=>owndUser.createdUsers)
    @JoinColumn({name:'user'})
    user;
}