import {Entity, Column, OneToMany} from 'typeorm';
import {Chapter} from  '../entity/chapter.entity';
import {Subject} from  '../entity/subject.entity';
import {Topic} from  '../entity/topic.entity';
import {common} from  '../entity/common.entity';
import {Question} from  '../entity/question.entity';


@Entity()
export class Exam extends common{
    @Column({type:'varchar'})
    examName:string;
    @OneToMany(type=>Subject,subject=>subject.exam)
    subjects:Subject[];
    @OneToMany(type=>Topic,topic=>topic.exam)
    topics:Topic[];
    @OneToMany(type=>Chapter,chapter=>chapter.exam)
    chapters:Chapter[];
    @OneToMany(type=>Question,question=>question.exam)
    questions:Question[];
} 