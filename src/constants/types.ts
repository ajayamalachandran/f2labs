export const TYPE={
    UserRepository:Symbol.for('UserRepository'),
    JwtRepository:Symbol.for('JwtRepository'),
    JwtTokenService:Symbol.for('JwtTokenService'),
    ExamRepository:Symbol.for('ExamRepository'),
    SubjectRepository:Symbol.for('SubjectRepository'),
    TopicRepository:Symbol.for('TopicRepository'),
    ChapterRepository:Symbol.for('ChapterRepository'),
    QuestionRepository:Symbol.for('QuestionRepository'),
    StudentQuestionRepository:Symbol.for('StudentQuestionRepository')
}