import {AsyncContainerModule} from 'inversify';
import {Repository,getConnection} from 'typeorm';
import {TYPE} from './constants/types';
import {UserController,AuthController,ExamController,SubjectController,TopicController,ChapterController,QuestionController,StudentQuestionController,ReportsController} from './controllers';
import {User} from './entity/user.entity'; 
import {Chapter} from './entity/chapter.entity'; 
import {common} from './entity/common.entity'; 
import {Exam} from './entity/exam.entity'; 
import {JwtToken} from './entity/jwtToken.entity'; 
import {Question} from './entity/question.entity'; 
import {StudentQuestion} from './entity/studentquestion.entity'; 
import {Subject} from './entity/subject.entity'; 
import {Topic} from './entity/topic.entity'; 
import {dbConnection} from './db';
import {JwtService} from './services';
export const bindings= new AsyncContainerModule (async(bind) =>{
    await dbConnection();
    await UserController;
    await AuthController;
    await ExamController;
    await SubjectController;
    await TopicController;
    await ChapterController;
    await QuestionController;
    await StudentQuestionController;
    await ReportsController;
    // Service
    bind<JwtService>(TYPE.JwtTokenService).to(JwtService);
    // Repo
    bind<Repository<User>>(TYPE.UserRepository).toDynamicValue(() => {
        const conn = getConnection();
            return conn.getRepository(User);
    }).inRequestScope();

    bind<Repository<JwtToken>>(TYPE.JwtRepository).toDynamicValue(() => {
        const conn = getConnection();
            return conn.getRepository(JwtToken);
    }).inRequestScope();

    bind<Repository<Exam>>(TYPE.ExamRepository).toDynamicValue(() => {
        const conn = getConnection();
            return conn.getRepository(Exam);
    }).inRequestScope();

    bind<Repository<Subject>>(TYPE.SubjectRepository).toDynamicValue(() => {
        const conn = getConnection();
            return conn.getRepository(Subject);
    }).inRequestScope();

    bind<Repository<Topic>>(TYPE.TopicRepository).toDynamicValue(() => {
        const conn = getConnection();
            return conn.getRepository(Topic);
    }).inRequestScope();

    bind<Repository<Chapter>>(TYPE.ChapterRepository).toDynamicValue(() => {
        const conn = getConnection();
            return conn.getRepository(Chapter);
    }).inRequestScope();

    bind<Repository<Question>>(TYPE.QuestionRepository).toDynamicValue(() => {
        const conn = getConnection();
            return conn.getRepository(Question);
    }).inRequestScope();

    bind<Repository<StudentQuestion>>(TYPE.StudentQuestionRepository).toDynamicValue(() => {
        const conn = getConnection();
            return conn.getRepository(StudentQuestion);
    }).inRequestScope();
})