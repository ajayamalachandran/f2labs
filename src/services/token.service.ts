import {Repository,getConnection} from 'typeorm';
import {BaseHttpController} from 'inversify-express-utils';
import {injectable} from 'inversify';
import {JwtToken} from '../entity/jwtToken.entity';
export class JwtService extends BaseHttpController{
    _jwtRepository=getConnection().getRepository(JwtToken);
    async isValidToken(encoded){
        this._jwtRepository.findOne({where:{jwt:encoded,status:1}}).then((validation)=>{
            if(validation){return true}else{return false}
        }).catch(error=>{
            return false;
        })
    }
    async invalidateToken(encoded:string){
       return await this._jwtRepository.update({jwtToken:encoded},{status:2}).then((result)=>{
            if(result){
                return this.json({
                    result
                })
            }else {
                return this.json({
                    message:"Request Failed"
                })
            }
        }).catch((error)=>{
            return this.json({
                error:error,
                message:"Request Failed"
            })
        })
    }
}